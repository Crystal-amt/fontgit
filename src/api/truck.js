// import * as request from '@/utils/request'

// export default {
//   getDriversdemand() {
//     const url = '/api/driversdemand'
//     return request.get(url)
//   }
// }

import request from '@/utils/request'

export default {
  getDriversdemand() {
    return request({
      url: '/api/driversdemand',
      method: 'get'
    })
  },
  getMap() {
    return request({
      url: '/api/map',
      // url: '/t/truckscheduling/mapjson/',
      method: 'get'
    })
  },
  getTruckproduce() {
    return request({
      url: '/api/truckproduce',
      // url: '/t/truckscheduling/produce/',
      method: 'get'
    })
  },
  // getTruckstates() {
  //   return request({
  //     url: '/api/truckstates',
  //     // url: '/api/truckproduce',
  //     method: 'get'
  //   })
  // },
  getShovelproduce() {
    return request({
      url: '/api/shovelproduce',
      method: 'get'
    })
  },
  getPilesum() {
    return request({
      url: '/api/pilesum',
      // url: '/t/truckscheduling/pilesum/',
      method: 'get'
    })
  },
  getTrucksum() {
    return request({
      url: '/api/trucksum',
      method: 'get'
    })
  },
  getQueue() {
    return request({
      url: '/api/queue',
      method: 'get'
    })
  }
}
