import request from '@/utils/request'

export function getPath(data) {
  return request({
    url: '/vue-admin-template/movecar/getPath',
    // url: 'https://851edf02-46eb-43e6-828d-64c7e483ea41.bspapp.com/http/getChangeLog',
    // url: 'https://172.24.210.96:8443/api/djx',
    method: 'get',
    data
  })
}
