import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router)

/* Layout 导航栏 */
// import Layout from '@/layout'

/**
 * Note: sub-menu only appear when route children.length >= 1
 * Detail see: https://panjiachen.github.io/vue-element-admin-site/guide/essentials/router-and-nav.html
 *
 * hidden: true                   if set true, item will not show in the sidebar(default is false)
 * alwaysShow: true               if set true, will always show the root menu
 *                                if not set alwaysShow, when item has more than one children route,
 *                                it will becomes nested mode, otherwise not show the root menu
 * redirect: noRedirect           if set noRedirect will no redirect in the breadcrumb
 * name:'router-name'             the name is used by <keep-alive> (must set!!!)
 * meta : {
    roles: ['admin','editor']    control the page roles (you can set multiple roles)
    title: 'title'               the name show in sidebar and breadcrumb (recommend set)
    icon: 'svg-name'/'el-icon-x' the icon show in the sidebar
    breadcrumb: false            if set false, the item will hidden in breadcrumb(default is true)
    activeMenu: '/example/list'  if set path, the sidebar will highlight the path you set
  }
 */

/**
 * constantRoutes
 * a base page that does not have permission requirements
 * all roles can be accessed
 */
export const constantRoutes = [
  {
    path: '/login',
    component: () => import('@/views/login/index'),
    hidden: true
  },

  {
    path: '/404',
    component: () => import('@/views/404'),
    hidden: true
  },
  // {
  //   path: '/',
  //   component: () => import('@/views/dashboard/index')
  // },
  // {
  //   path: '/2dMap',
  //   name: '2dMap',
  //   component: () => import('@/views/2dMap/index')
  // },
  {
    path: '/',
    component: () => import('@/views/2dMap/index')
  },
  {
    path: '/dashboard',
    name: 'dashboard',
    component: () => import('@/views/dashboard/index')
  },

  // {
  //   path: '/',
  //   component: Layout,
  //   redirect: '/MapShow',
  //   children: [
  //     {
  //       path: 'MapShow',
  //       name: 'MapShow',
  //       component: () => import('@/views/MapShow'),
  //       meta: { title: '地图展示', icon: 'form' }
  //     }
  //   ]
  // },
  // {
  //   path: '/2dMapTest',
  //   component: Layout,
  //   // redirect: '/2dMapTest',
  //   name: '2dMapTest',
  //   meta: { title: '二维地图测试' },
  //   // component: () => import('@/views/2dMapTest/index'),
  //   children: [
  //     {
  //       path: '/MapTest1',
  //       component: () => import('@/views/2dMapTest/MapTest1'),
  //       meta: { title: '二维地图测试1' }
  //     }
  //   ]
  // },

  // {
  //   path: '/',
  //   // component: Layout,
  //   redirect: '/dashboard',
  //   children: [{
  //     path: 'dashboard',
  //     name: 'Dashboard',
  //     component: () => import('@/views/dashboard/index'),
  //     // meta: { title: '调度方案展示', icon: 'dashboard' }
  //   }]
  // },

  // {
  //   path: '/',
  //   component: Layout,
  //   children: [
  //     {
  //       path: 'userCenter',
  //       name: 'userCenter',
  //       component: () => import('@/views/userCenter/index'),
  //       meta: { title: '用户中心', icon: 'el-icon-s-help' }
  //     }
  //   ]
  // },
  // {
  //   path: '/',
  //   component: Layout,
  //   children: [
  //     {
  //       path: '2dMap',
  //       name: '2dMap',
  //       component: () => import('@/views/2dMap/index'),
  //       // meta: { title: '二维地图' }
  //     }
  //   ]
  // },
  // {
  //   path: '/2dMapTest',
  //   // component: Layout,
  //   name: '2dMapTest',
  //   meta: { title: '二维地图测试' },
  //   // component: () => import('@/views/2dMapTest/index'),
  //   children: [
  //     {
  //       path: '/MapTest1',
  //       component: () => import('@/views/2dMapTest/MapTest1'),
  //       meta: { title: '二维地图测试1' }
  //     }
  //     // {
  //     //   path: '/MapTest2',
  //     //   component: () => import('@/views/2dMapTest/MapTest2'),
  //     //   meta: { title: '二维地图测试2'}
  //     // },
  //   ]
  // },
  // {
  //   path: '/ProductionSchedule',
  //   component: Layout,
  //   // redirect: '/ProductionSchedule/vehicle',
  //   name: 'ProductionSchedule',
  //   meta: {
  //     title: '生产调度控制台',
  //     icon: 'nested'
  //   },
  //   children: [
  //     {
  //       path: '/vehicle',
  //       name: 'vehicle',
  //       meta: { title: '矿卡调度' },
  //       component: () => import('@/views/ProductionSchedule/vehicle/index'),
  //       children: [
  //         {
  //           path: '/VehicleSchedule',
  //           component: () => import('@/views/ProductionSchedule/vehicle/VehicleSchedule'),
  //           meta: { title: '调度方案' }
  //         },
  //         {
  //           path: '/VehicleShow',
  //           component: () => import('@/views/ProductionSchedule/vehicle/VehicleShow'),
  //           meta: { title: '调度方案展示' }
  //         }
  //       ]
  //     },
  //     {
  //       path: 'shovel',
  //       component: () => import('@/views/ProductionSchedule/shovel/index'),
  //       name: 'shovel',
  //       meta: { title: '铲车调度' }
  //     },
  //     {
  //       path: 'water',
  //       component: () => import('@/views/ProductionSchedule/water/index'),
  //       name: 'water',
  //       meta: { title: '洒水调度' }
  //     },
  //     {
  //       path: 'maintenance',
  //       component: () => import('@/views/ProductionSchedule/maintenance/index'),
  //       name: 'maintenance',
  //       meta: { title: '维修调度' }
  //     },
  //     {
  //       path: 'gas',
  //       component: () => import('@/views/ProductionSchedule/gas/index'),
  //       name: 'gas',
  //       meta: { title: '加油调度' }
  //     }
  //     // {
  //     //   path: 'drill',
  //     //   component: () => import('@/views/ProductionSchedule/drill/index'),
  //     //   name: 'drill',
  //     //   meta: { title: '钻孔调度' }
  //     // },
  //     // {
  //     //   path: 'explosion',
  //     //   component: () => import('@/views/ProductionSchedule/explosion/index'),
  //     //   name: 'explosion',
  //     //   meta: { title: '爆破调度' }
  //     // },
  //   ]
  // },

  // 404 page must be placed at the end !!!
  { path: '*', redirect: '/404', hidden: true }
]

// const createRouter = () => new Router({
//   // mode: 'history', // require service support
//   scrollBehavior: () => ({ y: 0 }),
//   routes: constantRoutes
// })

// const router = createRouter()

// // Detail see: https://github.com/vuejs/vue-router/issues/1234#issuecomment-357941465
// export function resetRouter() {
//   const newRouter = createRouter()
//   router.matcher = newRouter.matcher // reset router
// }
const router = new Router({
  routes: constantRoutes
})

export default router
