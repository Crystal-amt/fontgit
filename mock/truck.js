// 模拟数据
const driversdemandData = require('../data/driversdemand.json')
const mapData = require('../data/map.json')
const shovelproduceData = require('../data/shovelproduce.json')
const truckproduceData = require('../data/truckproduce.json')
// const truckstatesData = require('../data/truckstates.json')
const pilesumData = require('../data/pilesum.json')
const trucksumData = require('../data/trucksum.json')
const queueData = require('../data/queue.json')
// const shovelstatesData = require('../data/shovelstatesData.json')

module.exports = [
  {
    url: '/api/driversdemand',
    type: 'get',
    response: config => {
      return driversdemandData
    }
  },
  {
    url: '/api/map',
    type: 'get',
    response: config => {
      return mapData
    }
  },
  {
    url: '/api/truckproduce',
    type: 'get',
    response: config => {
      return truckproduceData
    }
  },
  // {
  //   url: '/api/truckstates',
  //   type: 'get',
  //   response: config => {
  //     return truckstatesData
  //   }
  // },
  {
    url: '/api/shovelproduce',
    type: 'get',
    response: config => {
      return shovelproduceData
    }
  },
  {
    url: '/api/pilesum',
    type: 'get',
    response: config => {
      return pilesumData
    }
  },
  {
    url: '/api/trucksum',
    type: 'get',
    response: config => {
      return trucksumData
    }
  },
  {
    url: '/api/queue',
    type: 'get',
    response: config => {
      return queueData
    }
  }
]
