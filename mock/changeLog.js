const Mock = require('mockjs')

// const data = Mock.mock({
//   'items|5': [{
//     id: '@id',
//     content: '@sentence(10, 20)',
//     'status|1': ['published', 'draft', 'deleted'],
//     author: 'name',
//     display_time: '@datetime',
//     pageviews: '@integer(300, 5000)'
//   }]
// })

module.exports = [
  {
    url: '/vue-admin-template/changeLog/getList',
    type: 'get',
    response: config => {
      return {
        code: 200,
        msg: '',
        data: {
          data: [
            { '_id': '612e2ecb1b51d10001b8f1ad', 'content': '1#卡车请求加油', 'timestamp': '14:00' },
            { '_id': '61b6e72cd00c060001339c27', 'content': '2#卡车请求换班', 'timestamp': '15:00' },
            { '_id': '61c4752c39bf1000016e4439', 'content': '3#卡车突发状况', 'timestamp': '16:00' },
            { '_id': '621ce37f3da49e0001b419b1', 'content': '4#卡车需要维修', 'timestamp': '17:00' }
          ],
          data1: [
            { '_id': '612e2ecb1b51d10001b8f1ad', 'content': '1#铲车请求加油', 'timestamp': '14:00' },
            { '_id': '61b6e72cd00c060001339c27', 'content': '2#铲车请求换班', 'timestamp': '15:00' },
            { '_id': '61c4752c39bf1000016e4439', 'content': '3#铲车突发状况', 'timestamp': '16:00' },
            { '_id': '621ce37f3da49e0001b419b1', 'content': '4#铲车需要维修', 'timestamp': '17:00' }
          ]
        }
      }
    }
  }
]
